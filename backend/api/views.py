from django.shortcuts import render
from requests import post

from django.core import serializers

from trash.models import Trash

from .models import Job
from .serializers import JobSerilizer

from rest_framework.views import APIView
from rest_framework.response import Response

class ListOrCreateJobView(APIView):
    
    
    def get(self, request, format=None):
        """
        Return a list of all users.
        """
        job=Job.objects.all()
        
        serializers=JobSerilizer(job,many=True)
        return Response(serializers.data)
     
    def post(self,request,format=None):
        data = request.data
        
        serializers = JobSerilizer(data=data)

        serializers.is_valid(raise_exception=True)
        job = Job.objects.create(**serializers.validated_data)
        job.save()
        return Response({'status': 201})
    

class JobDetailedView(APIView):
    def get(self, request, pk):
        """
        Return a list of single user
        """
        job =Job.objects.get(id =pk)
        serializers=JobSerilizer(job)
        return Response(serializers.data)
    # update
    def put(self, request, pk):
        data = request.data
       # raise Exception (data)
        serializers = JobSerilizer(data=data)
        serializers.is_valid(raise_exception=True)
        Job.objects.update(**serializers.validated_data)
        return Response({'status': 200})
#delete
    def delete(self,request,pk):
        job =Job.objects.get(id = pk)
        serializer = JobSerilizer(job)
        trash = Trash.objects.create(**serializer.data)
        trash.save()
        job.delete()
    
        return Response({"status":202})
    
class TrashJobsView(APIView):
    
    
    def get(self, request, format=None):
        """
        Return a list of all users.
        """
        job=Job.objects.filter(isActive=False).values()
        
        serializers=JobSerilizer(job,many=True)
        return Response(serializers.data)





