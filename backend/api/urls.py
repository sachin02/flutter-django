from django.urls import path

#from api.views import JobGetCreate, JobUpdateDelete
from api.views import  JobDetailedView,  ListOrCreateJobView,TrashJobsView


urlpatterns = [
   path('jobs',ListOrCreateJobView.as_view()),
   path( 'job/<int:pk>', JobDetailedView.as_view()),
  # path( 'trash', TrashJobsView.as_view()),
  
]
