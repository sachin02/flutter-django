from django.shortcuts import render
from requests import post

from .models import Trash
from .serializers import TrashSerilizer

from rest_framework.views import APIView
from rest_framework.response import Response

class TrashView(APIView):
    
    
    def get(self, request, format=None):
        """
        Return a list of all users.
        """
        trash=Trash.objects.all()
        
        serializers=TrashSerilizer(trash,many=True)
        return Response(serializers.data)
     