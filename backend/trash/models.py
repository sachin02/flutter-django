

# Create your models here.
from django.db import models 


# Create your models here.
class Trash(models.Model):
    
    title = models.CharField(max_length =50)
    company =models.CharField(max_length =60)
    website=models.CharField(max_length= 50)
    address=models.CharField(max_length=50)
    amount=models.IntegerField(max_length =50)
    intershipStipend=models.IntegerField(max_length=50)
    totalExperience=models.IntegerField(max_length=50)
    experienceMonth=models.IntegerField( max_length=40)
    LastDateToApply=models.DateTimeField(auto_now=True)
    DriveDate=models.IntegerField(max_length=40)
    DriveLocation=models.CharField(max_length=40)
    isDone=models.BooleanField(default=False)
    image=models.ImageField(default=None,null=True)
    
    
    def __str__(self):
      return self.title[:50]
    
    
    
       